
#include <iostream>
#include <limits>
#include "std_lib_facilities.h"

void DecimalToUnary(int val);


int main()
{
    int val, theOption;
    
    cout << "Melyik számot szeretné unárisan látni?\n";
    
    do
    {
        cin >> val;
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
        
    }while(cin.fail());
    
    DecimalToUnary(val);
    
    return 0;
    
}

void DecimalToUnary(int val)
{
    for(int i=0; i<val; i++)
    {
        cout << "1";
    }
    cout << "\n";
}
