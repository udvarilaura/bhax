import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import javax.swing.JFrame;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JPanel;



public class FullscreenJava {
   public static void main(String[] args) {
      //char exitkey = 'x';
      GraphicsEnvironment graphics;
      graphics = GraphicsEnvironment.getLocalGraphicsEnvironment();
      GraphicsDevice device = graphics.getDefaultScreenDevice();
      JFrame frame = new JFrame("Fullscreen");
      JPanel panel = new JPanel();
      JLabel label = new JLabel("", JLabel.CENTER);

      label.setText("To get rid of me, use your mouse or bring me pizza!");
      label.setOpaque(true);
      frame.add(panel);
      frame.add(label);

      frame.setUndecorated(true);
      frame.setResizable(false);
      device.setFullScreenWindow(frame);
      label.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent e)
            {
                frame.dispose(); //A címkére kattintva kiléphetünk az ablakból
            }
        });
   }
}