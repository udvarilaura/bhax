#include <iostream>

template <typename ValueType> // fordítási időben ezek a ValueType-ek int-ek lesznek - a mainben jelölöm
class BinTree {

protected:
    class Node {
        
    private:
        ValueType value;
        Node *left;
        Node *right;
        int count{0}; //azt számolja, hogy a karakter hányszor szerepelt (inicializálásnak számít)
        
        //Todo rule of five: ha egy osztályban van pointer (*), akkor kell bele destruktor is.
        // Itt tiltjuk a másoló konstruktort és a másoló értékadást
        Node(const Node &); //fölveszem a Node konstruktort, ami kap egy konstans Node referenciát
        Node & operator=(const Node &);
        Node(Node &&);
        Node & operator=(Node &&);
        
    public: // ez lesz a konstrukorunk, ami felépíti a Node-ot
        Node(ValueType value, int count=0): value(value), count(count), left(nullptr), right(nullptr) {} 
        //feltöltöm a Value-t a paraméterként kapott value-val, 
        //majd ott van a bal pointer, a jobb pointer. ezután jön a teste
        ValueType getValue() {return value;} // visszaadja az aktuálus típust
        Node * leftChild() {return left;} //implementálom a bal oldali gyermeket is
        Node * rightChild() {return right;} //majd a jobb oldalit is
        void leftChild(Node * node){left = node;} //a baloldali részfát a node-ra állítjuk
        void rightChild(Node * node){right = node;} // szintén a jobb oldalit
        int getCount() {return count;}
        void incCount(){++count;}// implementáljuk a countot, ennek az implementációja eggyel való növelés
    };

    Node *root; //A gyökér pointer lesz, mutatót várunk rá
    Node *treep;    //tree pointer, hol tartok a fában?
    int depth{0};//A fa mélységét fogom számolni
    
public:
    //A fa konstruktora, átadjuk neki a Node *rootban a default értéket (nullptr), 
    //ugyanezt a fapointerrel is. Eztuán azt mondjuk, 
    //hogy az osztály root tagjába tegye be a paraméterként kapott rootot,
    //A tree pointerbe a tree paraméterként kapott értéket
    BinTree(Node *root = nullptr, Node *treep = nullptr): root(root), treep(treep) {
        std::cout << "A Bináris fa konstruktora lefutott" << std::endl;
    }

    BinTree(const BinTree & regi){//másoló konstruktor:
        std::cout << "A Bináris fa másoló konstruktora lefutott" << std::endl;


        root = masol(regi.root, regi.treep);
    }

    Node * masol(Node *node, Node *treep) 
    {
        Node *newNode =nullptr;

        if(node) //ha a node nem levélelem == nem 0
        {
            newNode = new Node(node->getValue());
            
            newNode->leftChild(masol(node->leftChild(), treep)); //másoljuk és kiíratjuk  a bal gyereket
            // A newNode a bal gyerek beállítójának visszaadom, hogy a bal gyereket adja vissza
            newNode->rightChild(masol(node->rightChild(), treep)); // majd jobbal ugyanígy
        
            if(node == treep)//hogyha az eredeti fába a node pont oda mutat, ahova a treepointer,
                // akkor a másoló fában a fapointert az új node-ra állítom.
                this->treep = newNode;

        }
        return newNode;
        
    }


    BinTree & operator=(const BinTree & regi){//másoló értékadás:
        std::cout << "A Bináris fa másoló értékadása lefutott" << std::endl;

        BinTree tmp{};
        std::swap(*this, tmp);//A *thisben levő dolgok átmennek az ideiglenes binfába,
        // a tmp-ből a dolgok átmennek a *node-ba.
        //Amikor ez a blokk a végéhez ér, hívódik a destruktor, ami a temp-ben levő elemeket is törli
        return *this;
    }
    BinTree(BinTree && regi){//mozgató konstrukror
        std::cout << "A Bináris fa mozgató konstruktora lefutott" << std::endl;

        root = nullptr; //az aktuális rootot nullpointerre állítom
        *this = std::move(regi); //kikényszerítem, hogy a mozgató értékadás jelenjen meg

    }
    BinTree & operator=(BinTree && regi){//mozgató értékadás
        std::cout << "A Bináris fa mozgató  értékadása lefutott" << std::endl;

        std::swap(regi.root, root); //megcserélem mind a gyökeret, mind a fa pointert
        std::swap(regi.treep, treep);

        return *this;

    }




    //Eztuán látjuk a destruktort:
    ~BinTree(){
        std::cout << "A Bináris fa destruktora lefutott" << std::endl;
        deltree(root);
    }
    BinTree & operator<<(ValueType value);
    void print(){print(root, std::cout);}
    void print(Node *node, std::ostream & os);
    void deltree(Node *node); 

};

template <typename ValueType>
class ZLWTree : public BinTree<ValueType> {
public:
    //hívom az ősosztály konstruktorát, és annak átadom a rootot
    //hogy azzal inicializálja a gyerekosztály őséből kaptt rootját
    ZLWTree(): BinTree<ValueType>(new typename BinTree<ValueType>::Node('/')) {
        this->treep = this->root; //thissel minősítenünk kell a fapointert és a rootpointert, hogy megtalálja a fordító
    }
    ZLWTree & operator<<(ValueType value); //ahogy a fába, úgy a bináris fába is bele szeretném shiftelni a bemenetet, ezt referenciával oldom meg
    
    
};

template <typename ValueType>
BinTree<ValueType> & BinTree<ValueType>::operator<<(ValueType value)
{//láthattuk a megadását annak, ha egy másik osztály metódusát hívjuk meg
    if(!treep) {//ha az aktuálisan feldolgozott elem nem nélezik
       
        root = treep = new Node(value); //akkor új node-ot hozok létre
        
    } else if (treep->getValue() == value) {// ha a fa nodeja megvan, de épp az az elem, amit el akarok helyezni
        
        treep->incCount();//ezesetben implementálok (A node-ban levő count számát növelem)
    
    } else if (treep->getValue() > value) {
        
        if(!treep->leftChild()) { //ha még nics bal oldali gyereke
            
            treep->leftChild(new Node(value)); //akkor készítünk
            
        } else {
            
            treep = treep->leftChild(); //ha létezik, akkor arra lépek
            *this << value; //és újrahívom ezt a függvényt
        }
        
    } else if (treep->getValue() < value) {
        
        if(!treep->rightChild()) {
            
            treep->rightChild(new Node(value));
            
        } else {
            
            treep = treep->rightChild();
            *this << value;
        }
        
    }
    
    treep = root;
    
    return *this; //visszaadjuk a referenciát az objektumra
}


template <typename ValueType>
ZLWTree<ValueType> & ZLWTree<ValueType>::operator<<(ValueType value)
{
    
    if(value == '0') {
        
        if(!this->treep->leftChild()) {//megnézem, hogy van e 0-s (=bal oldali) gyereke
            //hivatkozunk a binfa osztályos node-ra
            typename BinTree<ValueType>::Node * node = new typename BinTree<ValueType>::Node(value);
            this->treep->leftChild(node); //A fapointerrel a bal gyerekbe node-ot teszünk
            this->treep = this->root; // és visszaugrom a gyökérbe
            
        } else {
            
            this->treep = this->treep->leftChild(); // a fapointerrel lépünk
        }
        
    } else {

        if(!this->treep->rightChild()) {//ugyanez fordítva
            
            typename BinTree<ValueType>::Node * node = new typename BinTree<ValueType>::Node(value);
            this->treep->rightChild(node);
            this->treep = this->root;
            
        } else {
            
            this->treep = this->treep->rightChild(); 
        }
        
    }
    
    return *this;
}

template <typename ValueType> //kiíratás
void BinTree<ValueType>::print(Node *node, std::ostream & os) 
{
    if(node) //ha a node nem levélelem == nem 0
    {
        ++depth;
        print(node->leftChild(), os); //kiíratjuk a bal gyereket az output streamre (os)
        
        for(int i{0}; i<depth; ++i) // bevesszük a mélységet is, amíg i kisebb amélységnél, addig az os-nek
            os << "-";            // kinyomok annyi "-"-et, amennyi a mélység
        os << node->getValue() << " " << depth << " " << node->getCount() << std::endl;     
        //majd kiíratjuk a node értékét, a méységet és az elem eddigi előfordulásának számát
        print(node->rightChild(), os); // kiíratjuk a jobbakat
        --depth;
    }
    
}

// csinálunk egy destruktort, ez törli a fát.
template <typename ValueType>
void BinTree<ValueType>::deltree(Node *node) 
{
    if(node)
    {
        deltree(node->leftChild());
        deltree(node->rightChild());
        
        delete node;//a gyerekek törlése után önmagát is törli a node (posztorded bejárás)
    }
    
}


int main(int argc, char** argv, char ** env)
{
    BinTree<int> bt;
    
    bt << 8 << 9 << 5 << 2 << 7; //ez lesz a BinTree bemenete, beleshifteljük a számokat
    
    bt.print();
    
    std::cout << std::endl;

    ZLWTree<char> zt;

    zt <<'1'<<'1'<<'1'<<'1'<<'1';
    
    zt.print();

    ZLWTree<char> zt2{zt}; //úgy készítem el a zt2 fát, hogy azt a zt fából másolom
    //ezt a másoló konstruktorunk fogja végezni

    ZLWTree<char> zt3;

    zt3 <<'0'<<'0'<<'0'<<'0'<<'0';
std::cout<< "***" << std::endl;
    zt = zt3; // így a zt-be rakja majd a zt3 fáját
std::cout<< "***" << std::endl;
    ZLWTree<char> zt4 = std::move(zt2); //ez is a mozgató konstruktort fogja hívni


}
