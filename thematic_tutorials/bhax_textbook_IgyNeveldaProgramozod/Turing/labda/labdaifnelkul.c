#include <stdio.h>
#include <curses.h>
#include <stdlib.h>
#include <unistd.h>

int
main (void)
{
    int xj = 0, xk = 0, yj = 0, yk = 0;

    WINDOW *ablak;
    ablak = initscr ();
    int mx;
    int my;

    for (;;)
    {

        getmaxyx ( ablak, my , mx );
        xj = (xj - 1) % mx;
        xk = (xk + 1) % mx;

        yj = (yj - 1) % my;
        yk = (yk + 1) % my;

        clear ();

        mvprintw (abs ((yj + (my - yk))),
                  abs ((xj + (mx - xk))), "labda");

        refresh ();
        usleep (100000);

    }
    return 0;
}
