#include <stdio.h>
int main(){
    int a = 20;
    int b = 10;
    printf( "Eredeti a=%i b=%i\n",a, b );

    a+=b;    // a = 30
    b=a-b;   // b = 30-10 = 20
    a-=b;    // a = 30-20 = 10
    printf( "a csere utáni értéke: %i\nb csere utáni értéke: %i\n", a, b);

    a = 20;    //alapértékek visszaállítása
    b = 10;

    int c;
    c = a;    // c = 20
    a = b;    // a = 10
    b = c;    // b = 20
    printf( "a csere utáni értéke: %i\nb csere utáni értéke: %i\n", a, b);

    printf( "a jelenlegi értéke: 5; b jelenlegi értéke 9.\n" );
    a = 5;    // 0101
    b = 9;    // 1001

    a = a ^ b;  // 1100 
    b = a ^ b;  // 0101
    a = a ^ b;  // 1001
    printf( "a csere utáni értéke: %i\nb csere utáni értéke: %i\n", a, b);
}