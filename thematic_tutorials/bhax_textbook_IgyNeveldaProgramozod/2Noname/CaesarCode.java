import java.util.Scanner;
import java.io.IOException;
import java.io.*;
import java.io.FileWriter;

public class CaesarCode {
    public static void main(String args[]) throws IOException{
        System.out.println("Kodolando szoveg:\n");
        Scanner input = new Scanner(System.in);
        String szoveg = input.nextLine();
        kodolas(szoveg);
    }
    public static void kodolas(String kodolt) throws IOException{
        char betu;
        int ascii;
        File myObj = new File("kodolt.txt");
        myObj.createNewFile();
        System.out.println("\nFile created: " + myObj.getName());
        try (FileWriter output = new FileWriter("kodolt.txt")) {
            for (int i=0; i<kodolt.length(); ++i){
                betu = kodolt.charAt(i);
                ascii = betu;
                if ((ascii >= 65 && ascii <=90) || (ascii >=97 && ascii <=122))
                {output.write(betu+2);}
                else output.write(betu);
            }
            output.write('\n');
        }
    }
}

