#include <stdio.h>
#include <stdlib.h>
//ez kesz az a pointer, amivel a memóriarészt elérjük. Tm-pointer, mutat valamilyen memóriarészre.
    //Pointer értéke: egy egész szám - Hexa-ban jelöljük mert egyszerűbb.
    //tmben valamilyen rekeszre mutatnak. A memóriaterületet megcímezzük, annak minden egyes bájtot külön meg kell címeznünk. A meriamérettől függő lesz a méret
    //így például a 32bites windows 2^32en memóriacímet tudott kezelni. A 64bites 2^64en memóriacímet.
    // azért kell 8 byte, hogy meg tudjunk címezni jó sokat
    //malloc fügvénnyel memóriát foglalunk a free storeban, erre nyilván pointerekkel (itt tm pointerrel) tudunk hivatkozini. Bewprogban még new operátorral tudtunk, de itt ilyen nincs (c!=c++)
//man3 malloc
/*foglaltunk 5 double* méretű csillagot, visszakapjuk a double* értékét, 8byte*5 = 40byteot foglalunk
    //|*|*|*|*|*|
    /* | | | | |
       | | | | |2.5
       | | | |-4
       | | |3
       | |-21.5 
       |
       stb

Indítunk majd egy for ciklust, lefoglaljuk az egyes karaktereket(fenti rajz), értéket adunk nekik for ciklussal, matekos bűvészkedés, aztán külön adunk értéket
tm[3][0] -elmegyünk a 4. elemig, és annak a 4. eleme. (-4 a rajzon)
*(*(tm+1)) - tm pointer, ha hozzáadunk, a memóriacímet növeljük, azaz a kövi memóriacímre lépünk. Itt elkérjük a tm+1 memóriacímét (-21.5) -ilyet ne csináljunk, de működik
*(*(tm+6)+2) működik az 5 oszlop ellett is, de memóriaszemetet kapunk (nem tudjuk, mi lesz itt, valaminek vagy a helye, vagy nem.)
    */
int
main ()
{
    int nr = 5;
    double **tm; 
    
    //printf("%p\n", &tm);
    
    if ((tm = (double **) malloc (nr * sizeof (double *))) == NULL)
    {
        return -1;
    }

    //printf("%p\n", tm);
    
    for (int i = 0; i < nr; ++i)
    {
        if ((tm[i] = (double *) malloc ((i + 1) * sizeof (double))) == NULL)
        {
            return -1;
        }

    }

   // printf("%p\n", tm[0]);    
    
for (int i = 0; i < nr; ++i)
        for (int j = 0; j < i + 1; ++j)
            tm[i][j] = i * (i + 1) / 2 + j;

    /*for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%f, ", tm[i][j]);
        printf ("\n");
    }*/

    tm[3][0] = 42.0;
    (*(tm+3))[1] = 43.0;	// mi van, ha itt hiányzik a külső ()
    *(tm[3] + 2) = 44.0;
    *(*(tm + 3) + 3) = 45.0;

    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%f, ", tm[i][j]);
        printf ("\n");
    }

    for (int i = 0; i < nr; ++i)
        free (tm[i]);

    free (tm); //visszadjuk végül a memóriát az oprendszernek

    return 0;
}
//valgrind program ./tm