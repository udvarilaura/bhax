public class Yoda {

	public static void main(String[] args) {

	System.out.println("Most összehasonlítjuk: 'Laura' & a='Laura')\r\n");

	String a = "Laura";
	if (a.equals("Laura"))
		System.out.println("A két sztring normál szintaxissal egyezik.");
	if ("Laura".equals(a))
		System.out.println("A két sztring Yoda szintaxissal egyezik.\r\n");

	System.out.println("Most összehasonlítjuk: 'Laura' & b=null\r\n");

	String b = null;
	if (!"Laura".equals(b)) 
		System.out.println("A két sztring Yoda szintaxissal nem egyezik.");
	if (!b.equals("Laura")) 
		System.out.println("A két sztring normál szintaxissal nem egyezik.");

	}

}
