class Madar 
     {

          public void repul() 
          {
               System.out.println("Madár: Sikeresen repülök!");
          }
     }

class Program 
     {
          public static void fgv ( Madar madar ) 
               {
                    madar.repul();
               }    
     }

class Sas extends Madar
     {
          public void repul()
               {
                    System.out.println("Sas: Sikeresen repülök!");
               }
     }

class Pingvin extends Madar 
     {}

class Strucc extends Madar 
     {}

class LiskovSert 
{

     public static void main ( String args[] )
     {
          Program program = new Program();
          Madar madar = new Madar();
          program.fgv(madar);

          Sas sas = new Sas();
          program.fgv(sas);

          Pingvin pingvin = new Pingvin();
          System.out.print("Pingvin mint ");
          program.fgv(pingvin);

          Strucc strucc = new Strucc();
          System.out.print("Strucc mint ");
          program.fgv(strucc);
     }
}
